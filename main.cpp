#include <QImage>
#include <QColor>
#include <QVector>
#include <iostream>
#include <cmath>

QColor closestColor(const QColor c, const QVector<QColor>& colors)
{
    auto indx = colors.size()+1;;
    double sd = 10000.0;
    for(auto i = 0ul; i < colors.size(); ++i)
    {
        const int r2 = colors[i].red(); const int g2 = colors[i].green(); const int b2 = colors[i].blue();
        const int r1 = c.red(); const int g1 = c.green(); const int b1 = c.blue();

        double dist = sqrt( ((r2-r1)*(r2-r1)) + ((g2-g1)*(g2-g1)) + ( (b2-b1)*(b2-b1) ) );
        if(dist < sd){sd = dist;indx = i;}

    }
    return colors[indx];
}

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        std::cout << "ERROR: invalid program arguments" << std::endl;
        return -1;
    }

    QImage img;
    QImage pal;
    QVector<QColor> colors;

    img.load(argv[1]);
    pal.load(argv[2]);

    for(auto x = 0; x < img.width(); ++x)
    {
        for(auto y = 0; y < img.height(); ++y)
        {
            auto p = pal.pixelColor(x,y);
            p.setAlpha(255);
            bool found = false;
            for(auto& i : colors)
            {
                if(i == p)
                {
                    found = true;
                }
            }
            if(!found)
            {
                colors.push_back(p);
            }
        }
    }


    QImage out = img;

    for(auto x = 0; x < out.width(); ++x)
    {
        for(auto y = 0; y < out.height(); ++y)
        {
            auto p = out.pixelColor(x,y);
            p.setAlpha(255);
            auto col = closestColor(p, colors);
            col.setAlpha(out.pixelColor(x,y).alpha());
            out.setPixelColor(x, y, col);
        }
    }

    out.save("ni.png");

    return 0;
}











